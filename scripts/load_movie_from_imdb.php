<?php 
function load_person($db, $id){
  $uri = 'https://www.imdb.com/name/nm'.str_pad($id , 7 , "0" , STR_PAD_LEFT) ;

  if(!file_exists(md5($uri))) {
    $html = file_get_contents($uri);
    if(!$html) exit;

    [$p1,$p2] = @explode('<script type="application/ld+json">',$html);

    //echo $p2;

    [$p3,$p4] = @explode('</script>',$p2);

    //echo $p3;
    if(!$p3) exit;

    $data = @json_decode($p3,true);

    if(!$data) exit;

    file_put_contents(md5($uri), $p3);
  } else {
    $data = @json_decode(file_get_contents(md5($uri)),true);
  }
  print_r($data);
  return $data;
}

function process_person($db, $person, $role, $MovieID){
  global $People;

  if($person['@type'] !== 'Person') return;
  $id = (int)(substr($person['url'], 8));

  if(!isset($People[$id])){
    $data = load_person($db,$id);

    $st = $db->prepare('insert OR IGNORE into people (id,name_ru,name_en, date_born,date_death, description_ru, description_en, img) values(:id,:name,:name,:date_born,:date_death,:description,:description,:img)');
    $st->bindParam(':id', $id, SQLITE3_INTEGER);
    $st->bindParam(':name', $person['name'], SQLITE3_TEXT);
    $st->bindParam(':description', $data['description'], SQLITE3_TEXT);
    $st->bindParam(':date_born', $data['birthDate'], SQLITE3_TEXT);
    $st->bindParam(':date_death', $data['deathDate'], SQLITE3_TEXT);
    $st->bindParam(':img', $data['image'], SQLITE3_TEXT);

    $st->execute();
    echo "[{$id}] {$person['name']} NEW\n"; 

  } else {
    $st = $db->prepare('update people set name_ru = :name, name_en = :name where id=:id');
    $st->bindParam(':id', $id, SQLITE3_INTEGER);
    $st->bindParam(':name', $person['name'], SQLITE3_TEXT);
    $st->execute();
    echo "[{$id}] {$person['name']} OLD\n"; 
  }
  

  echo "[{$MovieID}] [{$id}] [{$role}]\n";

  $st = $db->prepare("insert OR IGNORE into person_movie_roles (person_id,movie_id, role) values(:person_id,:movie_id,:role)");
  $st->bindParam(':movie_id', $MovieID, SQLITE3_INTEGER);
  $st->bindParam(':person_id', $id, SQLITE3_INTEGER);
  $st->bindParam(':role', $role, SQLITE3_TEXT);
  $st->execute();

}


$db = new SQLite3('test.db');

$People = [];
$results = $db->query('SELECT * FROM People');
while($res = $results->fetchArray(SQLITE3_ASSOC)){
  $People[$res['id']]=$res;
}

$Genres = [];
$results = $db->query('SELECT * FROM Genres');
while($res = $results->fetchArray(SQLITE3_ASSOC)){
  $Genres[$res['id']]=$res;
}

$Movies = [];
$results = $db->query('SELECT * FROM Movies');
while($res = $results->fetchArray(SQLITE3_ASSOC)){
  $Movies[$res['id']]=$res;
}


if(!file_exists(md5($_SERVER['argv'][1]))) {
  $html = file_get_contents($_SERVER['argv'][1]);
  if(!$html) exit;

  [$p1,$p2] = @explode('<script type="application/ld+json">',$html);

  //echo $p2;

  [$p3,$p4] = @explode('</script>',$p2);

  //echo $p3;
  if(!$p3) exit;

  $data = @json_decode($p3,true);

  if(!$data) exit;

  if($data['@type'] !== 'Movie') exit;

  file_put_contents(md5($_SERVER['argv'][1]), $p3);
} else {
  $data = @json_decode(file_get_contents(md5($_SERVER['argv'][1])),true);
}

//print_r($data);
if(!isset($data['director'][0])) {
  $data['director'] = [$data['director']];
}


$MovieId = (int)(substr($data['url'], 9));


$st = $db->prepare('delete from movies where id=:id');
$st->bindParam(':id', $MovieId, SQLITE3_INTEGER);
$st->execute();

$st = $db->prepare('delete from person_movie_roles where movie_id=:id');
$st->bindParam(':id', $MovieId, SQLITE3_INTEGER);
$st->execute();

$st = $db->prepare('delete from genres_movies where movie_id=:id');
$st->bindParam(':id', $MovieId, SQLITE3_INTEGER);
$st->execute();


$st = $db->prepare('insert into movies (id,img,content_rating,title,title_en,title_ru,description_en,description_ru,duration,year,date_published) 
  values(:id,:img,:content_rating,:title,:title_en,:title_ru,:description_en,:description_ru,:duration,:year,:date_published)');

// id 

 $st->bindParam(':id', $MovieId, SQLITE3_INTEGER);
 $st->bindParam(':img', $data['image'], SQLITE3_TEXT);


 $st->bindParam(':content_rating', $data['contentRating'], SQLITE3_TEXT);
 $st->bindParam(':title', $data['name'], SQLITE3_TEXT);
 $st->bindParam(':title_ru', $data['name'], SQLITE3_TEXT);
 $st->bindParam(':title_en', $data['name'], SQLITE3_TEXT);

 $st->bindParam(':description_ru', $data['description'], SQLITE3_TEXT);
 $st->bindParam(':description_en', $data['description'], SQLITE3_TEXT);
 
//     [duration] => PT2H17M
 [$h,$m] = explode('H',substr($data['duration'], 2,-1));
 $dur = $h*60 +$m;
 // echo $dur;
 $st->bindParam(':duration', $dur, SQLITE3_TEXT);
 
 $year = (int)(substr($data['datePublished'], 0,4));

 $st->bindParam(':date_published', $data['datePublished'], SQLITE3_TEXT);
 $st->bindParam(':year', $year, SQLITE3_INTEGER);
 $st->execute();
 //exit();

 //ratings 
 $rating = isset($data['aggregateRating']) ? (int) ((float)($data['aggregateRating']['ratingValue'])*10.0) : 0;

 $st = $db->prepare('insert into ratings (movie_id,system,rating) values(:id,:system,:rating)');
 $st->bindParam(':id', $MovieId, SQLITE3_INTEGER);
 $st->bindValue(':system', 'imdb', SQLITE3_TEXT);
 $st->bindParam(':rating', $rating, SQLITE3_INTEGER);
 $st->execute();

// genres 
foreach($data['genre'] as $genre) {
  $id = strtolower($genre);
  if(!isset($Genres[$id])){
    echo "[{$id}] {$genre} new\n"; 
    $st = $db->prepare('insert into genres (id) values(:id)');
    $st->bindParam(':id', $id, SQLITE3_TEXT);
    $st->execute();
  } 
  $st = $db->prepare('insert into genres_movies (genre_id,movie_id) values(:genre_id,:movie_id)');
  $st->bindParam(':movie_id', $MovieId, SQLITE3_INTEGER);
  $st->bindParam(':genre_id', $id, SQLITE3_TEXT);
  $st->execute();

}

$st = $db->prepare("delete from person_movie_roles where movie_id = :movie_id");
$st->bindParam(':movie_id', $MovieID, SQLITE3_INTEGER);
$st->execute();

// actors
foreach($data['actor'] as $person) {
  process_person($db,$person,'actor',$MovieId);
}

// creators
//print_r($data['creator']);

foreach($data['creator'] as $person) {
  process_person($db,$person,'creator',$MovieId);
}

// directors
foreach($data['director'] as $person) {
  process_person($db,$person,'director',$MovieId);
}

print_r($Movies);

?>
