package main

/*
import (
	"os"
	"os/signal"
	"syscall"
  "net/http"
  "fmt"
  "time"
	"encoding/json"
)

import "github.com/zserge/webview"

import	(
  "github.com/anacrolix/torrent"
	"github.com/anacrolix/missinggo"
	"github.com/anacrolix/log"
	"github.com/dustin/go-humanize"
)

import	(
	"github.com/julienschmidt/httprouter"
)
*/
import (
  "strings"
  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
)

/*
DATABASE

*/
type H map[string]interface{}

type Country struct {
  ID string `gorm:"primary_key"`
}

type Person struct {
  ID 						uint64 `gorm:"primary_key; auto_increment:false"`
	Img   			 	string `json:",omitempty"`
	DateBorn 			string `json:",omitempty"`
	DateDeath 		string `json:",omitempty"`
  NameEn				string    `json:",omitempty"`
  NameRu				string    `json:",omitempty"`
  DescriptionEn 	string `json:",omitempty"`
  DescriptionRu 	string `json:",omitempty"`
}


type Genre struct {
  ID string `gorm:"primary_key"`
}

type Movie struct {
  ID 							uint64 `gorm:"primary_key; auto_increment:false"`
  
  Img 						string `json:",omitempty"`

  ContentRating 	string `json:",omitempty"`
  Title 					string `json:",omitempty"`

  TitleEn 				string `json:",omitempty"`
  TitleRu 				string `json:",omitempty"`

  DescriptionEn 	string `json:",omitempty"`
  DescriptionRu 	string `json:",omitempty"`

  Duration 				uint16 `json:",omitempty"`
  Year 						uint16 `json:",omitempty"`
  DatePublished 	string `json:",omitempty"`

  Genres         []string `json:",omitempty"`
  Countries      []string `json:",omitempty"`

  Actors         []Person `json:",omitempty"`
  Directors      []Person `json:",omitempty"`
  Creators       []Person `json:",omitempty"`
  Ratings        map[string]uint16 `json:",omitempty"`

	Sources				 []Source `gorm:"many2many:sources_movies;" json:",omitempty"`
}


type Rating struct {
//	Movie Movie  `gorm:"foreignkey:MovieID; auto_increment:false"`
	MovieID uint64 `gorm:"primary_key; auto_increment:false; foreignkey:movie_id"`
	System string `gorm:"primary_key"`
	Rating uint16 
}

type PersonMovieRole struct {
	MovieID uint64 `gorm:"primary_key;auto_increment:false"`
	PersonID uint64 `gorm:"primary_key;auto_increment:false"`
	Role string `gorm:"primary_key"`
}

type Source struct {
	ID uint64 `gorm:"primary_key;auto_increment:false"`

	Name string

	MagnetLink string `gorm:"unique_key"`
	InfoHash string `gorm:"unique_key"`

	Quality string
	VideoDesc string
	AudioDesc string

	BytesCompleted uint64
	Length uint64

	ApproximateSize uint64
	Status string // 1-new, 2-get info, 3-downloading, 4-ready, 5-error
}




func (movie *Movie) GetRatings(db *gorm.DB) {
    var ratings []Rating
    db.Model(&Rating{}).Where("movie_id = ?",movie.ID).Find(&ratings);
    movie.Ratings = make(map[string]uint16)
    for _,r := range ratings {
      movie.Ratings[r.System] = r.Rating
    }
}
func (movie *Movie) GetGenres(db *gorm.DB) {
    data := ""
    row := db.Raw("select group_concat(genre_id) as data from genres_movies where movie_id=?",movie.ID).Row();
    row.Scan(&data)
    movie.Genres = strings.Split(data,",")
}
func (movie *Movie) GetCountries(db *gorm.DB) {
    data := ""
    row := db.Raw("select group_concat(country_id) as data from countries_movies where movie_id=?",movie.ID).Row();
    row.Scan(&data)
    movie.Countries = strings.Split(data,",")
}
func (movie *Movie) GetActors(db *gorm.DB) {
    db.Model(&Person{}).Where("ID in (select person_id from person_movie_roles where movie_id=? and role='actor')",movie.ID).Find(&(movie.Actors));
}
func (movie *Movie) GetDirectors(db *gorm.DB) {
    db.Model(&Person{}).Where("ID in (select person_id from person_movie_roles where movie_id=? and role='director')",movie.ID).Find(&(movie.Directors));
}
func (movie *Movie) GetCreators(db *gorm.DB) {
    db.Model(&Person{}).Where("ID in (select person_id from person_movie_roles where movie_id=? and role='creator')",movie.ID).Find(&(movie.Creators));
}
func (movie *Movie) GetSources(db *gorm.DB) {
    db.Model(&Source{}).Where("ID in (select source_id from sources_movies where movie_id=?)",movie.ID).Find(&(movie.Sources));
}


