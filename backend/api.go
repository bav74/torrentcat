package main


import (
	"os"
	"os/signal"
	"syscall"
  "net/http"
  "fmt"
  "time"
	"encoding/json"
)


//import "github.com/zserge/webview"

import	(
  "github.com/anacrolix/torrent"
	"github.com/anacrolix/missinggo"
	"github.com/anacrolix/log"
	"github.com/dustin/go-humanize"
)

import	(
	"github.com/julienschmidt/httprouter"
)

import (
  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
)

type MetaElementString struct {
	ID string
	Cnt uint64
}

type MetaElementUint16 struct {
	Year uint16
	Cnt uint64
}

func Meta(db *gorm.DB) func(http.ResponseWriter,  *http.Request,  httprouter.Params) { 
  return func(w http.ResponseWriter,  r *http.Request,  _ httprouter.Params) {
		var genres []MetaElementString
		var countries []MetaElementString
		var years []MetaElementUint16

		genreshash := make(map[string]uint64)
		db.Raw("SELECT * FROM meta_genres").Scan(&genres)
		for _,c := range genres {
			genreshash[c.ID] = c.Cnt
		}
		
		countrieshash := make(map[string]uint64)
		db.Raw("SELECT * FROM meta_countries").Scan(&countries)
		for _,c := range countries {
			countrieshash[c.ID] = c.Cnt
		}


		yearshash := make(map[uint16]uint64)
		db.Raw("SELECT * FROM meta_years").Scan(&years)

		for _,c := range years {
			yearshash[c.Year] = c.Cnt
		}

		w.Header().Set("Content-Type", "application/json")
		js, err := json.MarshalIndent( H{"movies": H{"countries": countrieshash, "genres": genreshash, "years": yearshash}}, " ", " ")
		if(err != nil) {
			w.Write([]byte("0"))
		}
  	w.Write(js)	  
  }
}

func Movies(db *gorm.DB) func(http.ResponseWriter,  *http.Request,  httprouter.Params) { 
  return func(w http.ResponseWriter,  r *http.Request,  _ httprouter.Params) {
		var movies []Movie
		var query map[string][]interface{}

//		log.Printf("q = %v",r.URL.Query()["query"]);

		err := json.Unmarshal([]byte(r.URL.Query()["query"][0]), &query)
		qu := db.Debug().Model(&Movie{}).Select("distinct ID, title, year, img")

  	if genres, ok1 := query["genres"]; ok1 && len(genres)>0 {
			qu = qu.Joins("join genres_movies on genres_movies.movie_id = movies.id and genres_movies.genre_id in(?)",genres)
		}

  	if countries, ok2 := query["countries"]; ok2 && len(countries)>0 {
			qu = qu.Joins("join countries_movies on countries_movies.movie_id = movies.id and countries_movies.country_id in(?)",countries)
		}

  	if years, ok := query["years"]; ok && len(years)>0 {
//			log.Printf("years= %v", years);  		
			qu = qu.Where("year in(?)", years)
		}
		qu.Find(&movies)
		w.Header().Set("Content-Type", "application/json")
		js, err := json.MarshalIndent(movies, " ", " ")
		if(err != nil) {
			w.Write([]byte("0"))
		}
  	w.Write(js)	  
  }
}


func Movie1(db *gorm.DB) func(http.ResponseWriter,  *http.Request,  httprouter.Params) { 
  return func(w http.ResponseWriter,  r *http.Request,  ps httprouter.Params) {
		var movie Movie
		db.First(&movie,ps.ByName("id"))
		
		movie.GetSources(db)

		movie.GetCreators(db)	
		movie.GetDirectors(db)	
		movie.GetActors(db)
		movie.GetGenres(db)
		movie.GetCountries(db)
		movie.GetRatings(db)


		w.Header().Set("Content-Type", "application/json")
		js, err := json.MarshalIndent(movie, " ", " ")
		if(err != nil) {
			w.Write([]byte("0"))
		}
  	w.Write(js)	  
  }
}


func exitSignalHandlers(notify *missinggo.SynchronizedEvent) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	for {
		log.Printf("close signal received: %+v", <-c)
		notify.Set()
	}
}


func processStats(t *torrent.Torrent, db *gorm.DB, source * Source) {
	go func() {
		for {		
			select {
				case <-t.GotInfo():
				  fmt.Printf("got info: %q: %s/%s\n",
					t.Name(),	
					humanize.Bytes(uint64(t.BytesCompleted())),	
					humanize.Bytes(uint64(t.Length())))

					source.BytesCompleted = uint64(t.BytesCompleted())
					source.Length = uint64(t.Length())
					source.Status = "downloading"
					db.Save(source)

					for i,f := range t.Files() {
						if(f==nil) {
							continue;
						}
						fmt.Printf("%s: [ %q ] %s\n",i,f.Path(),f.Length())
					}

					time.Sleep(time.Second)

				case <-t.Closed():	
			  	fmt.Printf("closed: %v\n",t.Name())
					source.Status = "stopped"
					db.Save(source)
			  	return;
			}	
		}		
	}()
}


func StartSource(db *gorm.DB, client *torrent.Client) func(http.ResponseWriter,  *http.Request,  httprouter.Params) { 
  return func(w http.ResponseWriter,  r *http.Request,  ps httprouter.Params) {
		var source Source


		qu := db.Debug().Model(&Source{}).Where("ID = ?",ps.ByName("id"))
		qu.First(&source)

		t, err := client.AddMagnet(source.MagnetLink)
		log.Printf("start [%v] err:%v",t.InfoHash().HexString(),err);
		if err == nil {
			go func() {
				<-t.GotInfo()
				t.DownloadAll()
				processStats(t,db,&source)
			}()		
		}

		w.Header().Set("Content-Type", "application/json")
		js, err := json.MarshalIndent(source, " ", " ")
		if(err != nil) {
			w.Write([]byte("0"))
		}
  	w.Write(js)	  
  }
}

func StopSource(db *gorm.DB, client *torrent.Client) func(http.ResponseWriter,  *http.Request,  httprouter.Params) { 
  return func(w http.ResponseWriter,  r *http.Request,  ps httprouter.Params) {
		var source Source

		qu := db.Debug().Model(&Source{}).Where("ID = ?",ps.ByName("id"))
		qu.First(&source)

		t, err := client.AddMagnet(source.MagnetLink)
		log.Printf("stop [%v] err:%v",t.InfoHash().HexString(),err);
		if err == nil {
			go func() {
				t.Drop();
				log.Printf("drop [%v] err:%v",t.InfoHash().HexString(),err);
				source.Status = "stopped"
				db.Save(source)

			}()		
		}

		w.Header().Set("Content-Type", "application/json")
		js, err := json.MarshalIndent(source, " ", " ")
		if(err != nil) {
			w.Write([]byte("0"))
		}
  	w.Write(js)	  
  }
}



func main() {

	clientConfig := torrent.NewDefaultClientConfig()
	var stop missinggo.SynchronizedEvent
	defer func() {
		stop.Set()
	}()

	client, err := torrent.NewClient(clientConfig)
	if err != nil {
		fmt.Printf("serve error %p",err);
	}
	defer client.Close()
	go exitSignalHandlers(&stop)


 db, err := gorm.Open("sqlite3", "test.db")
  if err != nil {
    panic("failed to connect database")
  }
  defer db.Close()
  db = db.Debug();

 	router := httprouter.New()
  router.GET("/api/meta/", Meta(db))
  router.GET("/api/movies/", Movies(db)) 	

  router.GET("/api/movies/:id", Movie1(db)) 	

  router.PUT("/api/sources/:id/start", StartSource(db,client)) 	
  router.PUT("/api/sources/:id/stop", StopSource(db,client)) 	

/*
  router.GET("/api/sources/:id/start", StartSource(db,client)) 	
  router.GET("/api/sources/:id/stop", StopSource(db,client)) 	
 */

	router.NotFound = http.FileServer(http.Dir("./html"))
	go http.ListenAndServe("127.0.0.1:8181", router)

	func() {
		<-stop.C()
		client.Close()
	}()

 
}
